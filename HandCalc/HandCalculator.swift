//
//  HandCalculator.swift
//  HandCalc
//
//  Created by Jaron Heard on 6/21/14.
//  Copyright (c) 2014 Jaron Heard. All rights reserved.
//

import Foundation

func binomial(N: Int,K: Int) -> NSDecimalNumber
{
    // This function gets the total number of unique combinations based upon N and K.
    // N is the total number of items.
    // K is the size of the group.
    // Total number of unique combinations = N! / ( K! (N - K)! ).
    // This function is less efficient, but is more likely to not overflow when N and K are large.
    // Taken from:  http://blog.plover.com/math/choose.html
    var n = Int64(N)
    var k = Int64(K)
    var r: NSDecimalNumber = 1
    let d: Int64 = 0
    if k == 0 {
        return 1
    }
    
    if (k > n) {
        return 0
    }
    
    if (k == 0) {
        return 0;
    }
    
    for d in 1...k {
        n = n - 1
        r = r.decimalNumberByMultiplyingBy(NSDecimalNumber(longLong: n))
        r = r.decimalNumberByDividingBy(NSDecimalNumber(longLong: d))
    }
    return r
}

/* In probability theory and statistics, the hypergeometric distribution is a discrete probability distribution that describes the probability of k successes in n draws without replacement from a finite population of size N containing exactly K successes. This is in contrast to the binomial distribution, which describes the probability of k successes in n draws with replacement. */

func hypergeometric(successes: Int, draws: Int, totalSuccesses: Int, populationSize: Int)->Double {
    var k = successes
    var n = draws
    var K = totalSuccesses
    var N = populationSize
    var term1 = binomial(K,k)
    var term2 = binomial(N-K, n-k)
    var term3 = binomial(N, n)
    var probability = term1.decimalNumberByMultiplyingBy(term2).decimalNumberByDividingBy(term3).doubleValue
    return probability
}

func cumulativeHypergeometric(maxSuccesses: Int, draws: Int, totalSuccesses: Int, populationSize: Int)->Double {
    var total: Double = 0.0
    for x in 0...maxSuccesses {
        total += hypergeometric(x, draws, totalSuccesses, populationSize)
    }
    return total
}

class HandCalculator {
    var totalCards: Int
    var goodCards: Int
    var drawnCards: Int
    var possibleGoodCards: Int {
        return min(drawnCards,totalCards)
    }
    
    init(totalCards:Int, goodCards:Int, drawnCards:Int) {
        self.totalCards = totalCards
        self.goodCards = goodCards
        self.drawnCards = drawnCards
    }
    
    func exactlyArray() -> [Double] {
        var x: [Double] = []
        for index in 0...possibleGoodCards {
            x.append(hypergeometric(index,drawnCards,goodCards,totalCards))
        }
        return x
    }
}

class CachingHandCalculator: HandCalculator {
    /* This subclass adds functionality HandCalculator to ensure that the calculations for exactlyArray() are only calculated once for each set of parameters. If the results haven't been calculated, it calculates the array of results using the exactlyArray() method of the superclass and stores it in the dictionary. Otherwise, it returns the cached array from the dictionary. */
    
    var resultsCache: Dictionary<String, [Double]> = Dictionary()
    override func exactlyArray() -> [Double] {
        let key = "totalCards:\(totalCards)goodCards:\(goodCards)drawnCards:\(drawnCards)"
        if let x = resultsCache[key] {
            return x
        }   else {
            resultsCache[key] = super.exactlyArray()
            return resultsCache[key]!
        }
    }
}

func groupedZeroValues(exactlyArray: [Double]) -> [(Int, Int)] {
    var combine: Bool = false
    var currentElement: Double = 0.0, nextElement: Double = 0.0
    var nextIndex: Int = 0
    var beginningOfInterval: Int = 0, endOfInterval: Int = 0
    var intervalArray: [(Int, Int)] = []
    
    for currentIndex in 0..<exactlyArray.count {
        nextIndex = min(currentIndex+1,exactlyArray.count-1)
        currentElement = exactlyArray[currentIndex]
        nextElement = exactlyArray[nextIndex]
        if !combine { beginningOfInterval = currentIndex }
        combine = currentElement == 00 && nextElement == 0 && !(currentIndex == nextIndex)
        if !combine {
            endOfInterval = currentIndex
            intervalArray += [(beginningOfInterval, endOfInterval)]
        }
    }
    return intervalArray
}

func formatTableViewData(exactlyArray: [Double]) -> [(label: String,formattedValue: String, rawValue: Double)] {
    var intervalArray: [(Int, Int)] = groupedZeroValues(exactlyArray)
    var formattedData: [(label: String,formattedValue: String, rawValue: Double)] = []
    var label: String = ""
    var formattedValue: String = ""
    var rawValue: Double = 0.0
    var numberFormat: String = ""
    
    for (beginning, end) in intervalArray {
        assert(exactlyArray[beginning] == exactlyArray[end], "values combined were not equal")
        rawValue = exactlyArray[beginning]
        numberFormat = "%.2f%%"
        if rawValue == trunc(rawValue) { numberFormat = "%.0f%%" }
        formattedValue = NSString(format:numberFormat, rawValue*100)
        switch beginning {
            case 1 where end == 1:
                label = "\(beginning) Good"
            case end:
                label = "\(beginning) Good"
            default:
                label = "\(beginning)-\(end) Good"
        }
        formattedData += [(label: label, formattedValue: formattedValue, rawValue: rawValue)]
    }
    
    return formattedData
}
