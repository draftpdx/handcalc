//
//  ViewController.swift
//  HandCalc
//
//  Created by Seamus on 6/8/14.
//  Copyright (c) 2014 Tiny Heavy Laboratories. All rights reserved.
//

import UIKit

class MainViewController: UIViewController {
//   uib
    
    @IBOutlet var totalCardsDrag: DragLabel?
    @IBOutlet var goodCardsDrag: DragLabel?
    @IBOutlet var drawnCardsDrag : DragLabel?
    @IBOutlet var helpView : UIView? = nil
    @IBOutlet var mainView : UIView? = nil
    let calc: CachingHandCalculator = CachingHandCalculator(
        totalCards: 40,
        goodCards: 17,
        drawnCards: 7
    )
    
    override func viewDidLoad() {
        super.viewDidLoad()
        helpView!.hidden = true
        self.view.addSubview(helpView!)
        self.navigationItem.title = "Inputs"
        setDragLabelCurrentValues()
        setupDragLabelInteraction()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func setDragLabelCurrentValues() -> Void {
        totalCardsDrag?.currentValue = self.calc.totalCards
        goodCardsDrag?.currentValue = self.calc.goodCards
        drawnCardsDrag?.currentValue = self.calc.drawnCards
    }
    
    func setupDragLabelInteraction() -> Void {
        totalCardsDrag?.adjustRelatedDragLabels = totalCardsDragAdjust
        goodCardsDrag?.adjustRelatedDragLabels = goodCardsDragAdjust
        drawnCardsDrag?.adjustRelatedDragLabels = drawnCardsDragAdjust
    }
    
    // Jaron: here's an illustration of three different ways we can deal
    // with the issue of our IBOutlets being optionals.  The first way is my
    // current preference, I guess, but it's verbose and maybe I'll change
    // my mind at some point. -S
    
    func totalCardsDragAdjust() -> Void {
        if totalCardsDrag?.currentValue < goodCardsDrag?.currentValue {
            goodCardsDrag?.currentValue = totalCardsDrag!.currentValue
        }
        if totalCardsDrag?.currentValue < drawnCardsDrag?.currentValue{
            drawnCardsDrag?.currentValue = totalCardsDrag!.currentValue
        }
    }
    
    func goodCardsDragAdjust() -> Void {
        if goodCardsDrag?.currentValue > totalCardsDrag?.currentValue {
            totalCardsDrag?.currentValue = goodCardsDrag!.currentValue
        }
        self.goodCardsDrag?.currentValue = min(self.totalCardsDrag!.currentValue,
                                               self.goodCardsDrag!.currentValue);
    }
    
    func drawnCardsDragAdjust() -> Void {
        if drawnCardsDrag?.currentValue > totalCardsDrag?.currentValue {
            totalCardsDrag?.currentValue = drawnCardsDrag!.currentValue
        }
    }
    
    @IBAction func continuePressed() {
        calc.totalCards = self.totalCardsDrag!.currentValue
        calc.goodCards = self.goodCardsDrag!.currentValue
        calc.drawnCards = self.drawnCardsDrag!.currentValue
        var vc = OutputViewController(calc: self.calc)
        self.navigationController!.pushViewController(vc, animated: true)
    }
    
    @IBAction func helpPressed() {
        helpView?.hidden = false
    }
    
    
    @IBAction func dismissPressed() {
        helpView?.hidden = true
    }

}

