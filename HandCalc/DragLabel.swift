//
//  DragLabel.swift
//  HandCalc
//
//  Created by Seamus on 6/8/14.
//  Copyright (c) 2014 Tiny Heavy Laboratories. All rights reserved.
//

import UIKit

class DragLabel: UILabel {
    
    @IBInspectable var currentValue: Int = 0 {
        didSet {
            super.text = "\(self.currentValue)"
            if (self.currentValue < self.minValue) {
                self.minValue = self.currentValue
            }
            if (self.currentValue > self.maxValue) {
                self.maxValue = self.currentValue
            }
        }
    }
    
    @IBInspectable var maxValue: Int = 0 {
        didSet {
            if (self.currentValue > self.maxValue) {
                self.currentValue = self.maxValue
            }
            if (self.minValue > self.maxValue) {
                self.minValue = self.maxValue
            }
        }
    }
    
    @IBInspectable var minValue: Int = 0 {
        didSet {
            if (self.currentValue < self.minValue) {
                self.currentValue = self.minValue
            }
            if (self.maxValue < self.minValue) {
                self.maxValue = self.minValue
            }
        }
    }
    
    var startingTranslation: CGPoint = CGPoint.zeroPoint
    var startingValue: Int = 0
    var increaseValuePerPixel: Float = 0.0
    var decreaseValuePerPixel: Float = 0.0
    var adjustRelatedDragLabels: () -> Void = {}
        
    override init(frame: CGRect) {
        super.init(frame: frame)
        // Initialization code
        self.userInteractionEnabled = true
        self.addGestures()
    }
    
    required init(coder: NSCoder) {
        super.init(coder: coder)
        self.userInteractionEnabled = true
        self.addGestures()
    }
    
    func addGestures() -> Void {
        var gesture = UIPanGestureRecognizer(target: self, action: "respondToPanGesture:")
        self.addGestureRecognizer(gesture)
    }

    func respondToPanGesture(gesture: UIPanGestureRecognizer) -> Void {
        if gesture.state == .Ended {
            self.startingTranslation = .zeroPoint
            self.startingValue = 0
            self.increaseValuePerPixel = 0.0
            self.decreaseValuePerPixel = 0.0
            return
        }
        
        if gesture.state == .Began {
            self.startingTranslation = gesture.locationInView(self)
            self.startingValue = self.currentValue
            let superviewTranslation = gesture.locationInView(self.superview)
            
            let distanceToMax = self.maxValue - self.currentValue
            self.increaseValuePerPixel = Float(distanceToMax)/Float(superviewTranslation.y)
            
            let distanceToMin = self.currentValue - self.minValue
            self.decreaseValuePerPixel = Float(distanceToMin)/(Float(self.superview!.bounds.height) - Float(superviewTranslation.y))
        }
        let currentTranslation = gesture.locationInView(self)
        let vDelta = Float(currentTranslation.y) - Float(startingTranslation.y)
        if (vDelta < 0) {
            let increase = abs(Int(Float(vDelta) * increaseValuePerPixel))
            let newValue = self.startingValue + increase
            self.currentValue = (newValue < self.maxValue) ? newValue: self.maxValue
            adjustRelatedDragLabels()
        } else {
            let decrease = Int(Float(vDelta) * decreaseValuePerPixel)
            let newValue = self.startingValue - decrease
            self.currentValue = (newValue > self.minValue) ? newValue: self.minValue
            adjustRelatedDragLabels()
        }
    }
    
}
