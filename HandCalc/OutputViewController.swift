//
//  OutputViewController.swift
//  HandCalc
//
//  Created by Seamus on 6/8/14.
//  Copyright (c) 2014 Tiny Heavy Laboratories. All rights reserved.
//

import UIKit

class OutputViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    @IBOutlet var tableView: UITableView?
    @IBOutlet var sumLabel: UILabel?
    @IBOutlet var graphView : UIView?
    @IBOutlet var graphTotalView : UIView?
    @IBOutlet var graphTotalHeightConstraint : NSLayoutConstraint?
    let calc: CachingHandCalculator = CachingHandCalculator(totalCards: 0, goodCards: 0, drawnCards: 0)
    var results: [Double] = []
    var formattedResults: [(label: String, formattedValue: String, rawValue: Double)] = []
    
    convenience init(calc: CachingHandCalculator) {
        self.init(nibName: "OutputViewController", bundle: nil)
        self.results = calc.exactlyArray()
        self.formattedResults = formatTableViewData(results)
    }
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.updateGraph(animate: false)
        graphView?.layer.borderColor = UIColor.grayColor().CGColor // Can't set in IB, set here?
        self.navigationItem.title = "Results"
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return countElements(self.formattedResults)
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell: UITableViewCell = UITableViewCell(style: UITableViewCellStyle.Value1, reuseIdentifier: "goodCards")
        
        cell.textLabel.text = formattedResults[indexPath.row].label
        cell.detailTextLabel?.text = formattedResults[indexPath.row].formattedValue

        return cell
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath!) -> Void {
        self.updateGraph()
    }
    
    func tableView(tableView: UITableView, didDeselectRowAtIndexPath indexPath: NSIndexPath!) -> Void {
        self.updateGraph()
    }
    
    func updateGraph(animate: Bool = true) {
        let sumOfSelectedRows: Double = sumSelectedRows()
        updateGraphHeight(sumOfSelectedRows, animate: animate)
        updateGraphLabel(sumOfSelectedRows)
    }
    
    func updateGraphLabel(sum: Double) {
        var sumPercent: Double = sum * 100.0
        
        if sumPercent == 0.0 {
            self.sumLabel?.text = "Tap rows to sum"
        } else {
            self.sumLabel?.text = NSString(format:"%.1f%%", sumPercent)
        }
    }
    
    func updateGraphHeight(sum: Double, animate: Bool) {
        if let graphFrame = self.graphView?.frame {
            let navbarHeight: CGFloat = 64.0
            let graphHeight: CGFloat = graphFrame.height - navbarHeight
            let animationLength: NSTimeInterval = animate ? 0.2 : 0.0
            
            UIView.animateWithDuration(animationLength, {
                self.graphTotalHeightConstraint?.constant = graphHeight * CGFloat(sum)
                self.graphTotalView?.layoutIfNeeded()
            })
        }
    }
    
    func sumSelectedRows() -> Double {
        var sum: Double = 0.0
        if let selectedRows = self.tableView?.indexPathsForSelectedRows() {
            for indexPath in selectedRows as [NSIndexPath] {
                sum += self.formattedResults[indexPath.row].rawValue
            }
        }
        return sum
    }
    
}