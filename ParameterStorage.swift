//
//  ParameterStorage.swift
//  HandCalc
//
//  Created by Jaron Heard on 6/22/14.
//  Copyright (c) 2014 Tiny Heavy Laboratories. All rights reserved.
//

import Foundation

var appTotalCards = 40
var appGoodCards = 17
var appCardsDrawn = 7
var appResults = HandCalculator(totalCards: appTotalCards, goodCards: appGoodCards, cardsDrawn: appCardsDrawn)

