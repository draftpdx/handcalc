//
//  HandCalcTests.swift
//  HandCalcTests
//
//  Created by Seamus on 6/8/14.
//  Copyright (c) 2014 Tiny Heavy Laboratories. All rights reserved.
//

import XCTest

class PreordainTests: XCTestCase {
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
        
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testBinomial() {
        // This is an example of a functional test case.
        XCTAssertEqual(binomial(40, 0), 1)
        XCTAssertEqual(binomial(5, 2), 10)
        XCTAssertEqual(binomial(39, 7), 15380937)
        XCTAssertEqual(binomial(40, 7), 18643560)
        XCTAssertEqual(binomial(40, 6), 3838380)
    }
    
    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measureBlock() {
            // Put the code you want to measure the time of here.
        }
    }
    
}
